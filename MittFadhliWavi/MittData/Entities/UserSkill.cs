﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MittData.Entities
{
    public partial class UserSkill
    {
        public string UserSkillId { get; set; }
        public string Username { get; set; }
        public int? SkillId { get; set; }
        public int? SkillLevelId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }
    }
}
