﻿using Mitt.Commons.BaseModels;
using MittCommon.ViewModels;
using MittData.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MittRepository.Interface;
using MittData.Entities;

namespace MittRepository.Implementations
{
    public class SkilRepository : ISkillViewModel
    {
        private readonly MittContext _context;

        public SkilRepository (MittContext context)
        {
            _context=context;
        }

        public async Task<ResponseDto<List<SkillViewModel>>> GetAll()
        {
            try
            {
                List<SkillViewModel> data = await (from a in _context.TblMSkills
                                                   select new SkillViewModel
                                                   {
                                                       SkillId = a.SkillId,
                                                       SkillName = a.SkillName,
                                                       CreatedBy = a.CreatedBy,
                                                       CreatedOn = a.CreatedOn,
                                                       ModifiedBy = a.ModifiedBy,
                                                       ModifiedOn = a.ModifiedOn,
                                                       DeletedBy = a.DeletedBy,
                                                       DeletedOn = a.DeletedOn,
                                                       IsDelete = a.IsDelete
                                                   }).ToListAsync();
                return new ResponseDto<List<SkillViewModel>>(data);
            }
            catch (Exception ex)
            {

                return new ResponseDto<List<SkillViewModel>>(false, ex.Message);
            }
        }

        public async Task<ResponseDto<SkillViewModel>> GetById(int id)
        {
            try
            {


                SkillViewModel data = await (from a in _context.TblMSkills
                                             where a.SkillId == id
                                             select new SkillViewModel
                                             {
                                                 SkillId=a.SkillId,
                                                 SkillName=a.SkillName,
                                                 CreatedBy=a.CreatedBy,
                                                 CreatedOn=a.CreatedOn,
                                                 ModifiedBy=a.ModifiedBy,
                                                 ModifiedOn=a.ModifiedOn,
                                                 DeletedBy = a.DeletedBy,
                                                 DeletedOn = a.DeletedOn,
                                                 IsDelete = a.IsDelete
                                             }).FirstOrDefaultAsync();
                return new ResponseDto<SkillViewModel>(data);
            }
            catch (Exception ex)
            {
                return new ResponseDto<SkillViewModel>(false, ex.Message);
            }
        }


        public async Task<ResponseDto<SkillViewModel>> PostCreate(SkillViewModel data)
        {
            try
            {
                TblMSkill a = new TblMSkill
                {
                    SkillId= data.SkillId,
                    SkillName=data.SkillName,
                    CreatedBy=1,
                    CreatedOn=DateTime.Now,
                    ModifiedBy=data.ModifiedBy,
                    ModifiedOn=DateTime.Now,
                    DeletedBy=data.DeletedBy,
                    DeletedOn=data.DeletedOn,
                    IsDelete=data.IsDelete
                };

                _context.TblMSkills.Add(a);
                await _context.SaveChangesAsync();
                return new ResponseDto<SkillViewModel>();
            }
            catch (Exception ex)
            {

                return new ResponseDto<SkillViewModel>(false, ex.Message);
            }
        }
    }
}
