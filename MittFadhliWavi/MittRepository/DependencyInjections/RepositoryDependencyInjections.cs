﻿using Microsoft.Extensions.DependencyInjection;
using MittCommon.ViewModels;
using MittRepository.Implementations;
using MittRepository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace MittRepository.DependencyInjections
{
    public static class RepositoryDependencyInjections
    {
        public static IServiceCollection AddRepositoryServices(this IServiceCollection services)
        {
            services.AddTransient<ISkillViewModel, SkilRepository>();
            return services;
        }
    }
}
