﻿
using Mitt.Commons.BaseModels;
using MittCommon.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MittRepository.Interface
{
    public interface ISkillViewModel
    {
        Task<ResponseDto<List<SkillViewModel>>> GetAll();
        Task<ResponseDto<SkillViewModel>> GetById(int id);

        Task<ResponseDto<SkillViewModel>> PostCreate(SkillViewModel model);
    }
}
