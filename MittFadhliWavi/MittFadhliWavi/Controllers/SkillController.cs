﻿using Microsoft.AspNetCore.Mvc;
using MittCommon.ViewModels;
using MittRepository.Interface;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MittFadhliWavi.Controllers
{
    public class SkillController : Controller
    {
        private readonly ISkillViewModel _skillViewModel;
        public SkillController(ISkillViewModel skillViewModel)
        {
            _skillViewModel=skillViewModel;
        }

        public async Task <IActionResult> Index()
        {
            List<SkillViewModel> data = new List<SkillViewModel>();
            var response = await _skillViewModel.GetAll();
            if (response.IsSuccess)
            {
                foreach (var a in response.Data)
                {
                    SkillViewModel b = new SkillViewModel
                    {
                        SkillId = a.SkillId,
                        SkillName = a.SkillName,
                        CreatedBy = a.CreatedBy,
                        CreatedOn = a.CreatedOn,
                        ModifiedBy = a.ModifiedBy,
                        ModifiedOn = a.ModifiedOn,
                        DeletedBy = a.DeletedBy,
                        DeletedOn = a.DeletedOn,
                        IsDelete = a.IsDelete,
                    };
                    data.Add(b);
                }
            }
            return View();
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(SkillViewModel a, int id)
        {
            await _skillViewModel.PostCreate(a);
            return RedirectToAction("Index");
        }
    }
}
